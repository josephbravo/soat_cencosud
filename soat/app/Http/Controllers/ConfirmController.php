<?php

namespace App\Http\Controllers;

use App\Models\Data_pay_reports;
use Carbon\Carbon;
use App\ModelsConfirm;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ConfirmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = "GO IT!";

        return view('confirm', compact('json'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $now = new \DateTime();
        $date = Carbon::now();
        $tomorrow = $date->add(1, 'day');
        $email = $request->input('email');
        $celular = $request->input('celular');
        $respuesta = $request->input('respuesta');

        $nombres = "PAOLA ALEJANDRA";
        $apellidos = "ROJAS ZULUAGA";
        $documento = "1017217135";
        $placa = "XSM59D";
        $valor = "483950";


        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'css83lpw0bBc5jGw'; // trankey  
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $req["auth"]["seed"] = $seed;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["tranKey"] = $tranKey;
        $req["expiration"] = "2020-05-20T00:00:00 -05:00";

        $json = '{
            "buyer": {
              "name": "'.$nombres.'",
              "surname": "'.$apellidos.'",
              "email": "'.$email.'",
              "document": "'.$documento.'",
              "documentType": "CC",
              "mobile": "'.$celular.'"
            },
            "payment": {
              "reference": "'.$placa.$documento.$now->format('dmYHis').'",
              "description": "Pago SOAT",
              "amount": {
                "currency": "COP",
                "total": '.$valor.'
              }
            },
            "expiration": "'.$tomorrow.'",
            "ipAddress": "201.184.3.213",
<<<<<<< HEAD
            "returnUrl": "http://127.0.0.1:8000/controlpay",
=======
            "returnUrl": "http://127.0.0.1:8001/controlpay",
>>>>>>> fbec8245b7f457ad4d80e97c11ec23c8f7ff5f4f
            "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
            "paymentMethod": "",
            "skipResult": "True",

            "auth": {
              "login": "743eeee7c453349c31c09d82714f5c83",
              "tranKey": "'.$tranKey.'",
              "nonce": "'.$nonceBase64.'",
              "seed": "'.$seed.'"
             }
            }';

        $req = json_decode($json,true);

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            
            'timeout' => 600,
        ]);

        $response = $client->request('post', 'https://test.placetopay.com/redirection/api/session/', [ 'body' => json_encode($req) ]);

        $response = json_decode($response->getBody()->getContents(), true);

        $response_encode = json_encode($response["processUrl"], true);
        session_start();
        $requestId = json_encode($response["requestId"], true);

        $cambio1 = str_replace("\/\/","//", $response_encode);
        $cambio2 = str_replace("\/","/", $cambio1);
        $cambio3 = str_replace('"',"", $cambio2);

        $url = $cambio3;

        $date = Carbon::now();
        $pay_report = new Data_pay_reports;
        $pay_report->nombre="Joseph";
        $pay_report->apellido="Bravo";
        $pay_report->identificacion=$documento;
        $pay_report->email="prueba-connect@gmail.com";
        $pay_report->celular="3203509948";
        $pay_report->placa=$placa;
        $pay_report->marca="KYMCO";
        $pay_report->modelo="2017";
        $pay_report->linea="AGILITY DIGITAL 125";
        $pay_report->servicio="Particular";
        $pay_report->codentidad="54445";
        $pay_report->codigo_pto_venta="01";
        $pay_report->valor=$valor;
        $pay_report->requestid=$requestId;
        $pay_report->referencia="SOAT123456";
        $pay_report->estado="COTIZACION";
        $pay_report->razon="";
        $pay_report->mensaje="";
        $pay_report->fecha=$date;
        $pay_report->referido=$respuesta;
        $pay_report->metodo_pago="VISA";
        $pay_report->num_poliza="123456789";
        $pay_report->fecha_vigencia=$date;
        $pay_report->fecha_expiracion=$date;
        $pay_report->numero_transaccion="123456";
        $pay_report->entidad_recaudadora="BANCOPRUEBA";
        $pay_report->updated_at=$date;
        $pay_report->created_at=$date;

        $pay_report->save();


        return redirect()->away($url);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsConfirm $modelsConfirm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsConfirm $modelsConfirm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsConfirm $modelsConfirm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsConfirm  $modelsConfirm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsConfirm $modelsConfirm)
    {
        //
    }
}
