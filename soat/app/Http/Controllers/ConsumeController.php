<?php

namespace App\Http\Controllers;

use App\Models\Data_pay_reports;
use App\Models\Cotizaciones;
use Carbon\Carbon;
use App\ModelsConsume;
use Illuminate\Http\Request;

class ConsumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $placa = $request->input('placa');
        $tipo_documento = $request->input('tipo_doc');
        $documento = $request->input('identificacion');

        $json = $placa;

        //$status_pay = Data_pay_reports::where('placa',$placa)->get('estado_transaccion')->last();
        //$status= json_encode($status_pay["estado_transaccion"], true);
        //$status_pay = str_replace('"',"", $status);

        //if ($status_pay == "PENDING_PAY") {
        //    return view('layouts/control_pay');
        //}

        $date = Carbon::now();
        $cotizacion = new Cotizaciones;
        $cotizacion->nombre="Joseph";
        $cotizacion->apellido="Bravo";
        $cotizacion->identificacion=$documento;
        $cotizacion->tipo_identificacion=$tipo_documento;
        $cotizacion->placa=$placa;
        $cotizacion->marca="KYMCO";
        $cotizacion->linea="AGILITY DIGITAL 125";
        $cotizacion->modelo="2016";
        $cotizacion->cilindraje="125";
        $cotizacion->valor="483.950";
        $cotizacion->fecha=$date;
        $cotizacion->requestid="123456";
        $cotizacion->referencia="SOAT123456";
        $cotizacion->estado='COTIZACION';
        $cotizacion->razon='';
        $cotizacion->mensaje='';
        $cotizacion->updated_at=$date;
        $cotizacion->created_at=$date;
        
        $cotizacion->save();


        return view('consume', compact('json'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsConsume  $modelsConsume
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsConsume $modelsConsume)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsConsume  $modelsConsume
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsConsume $modelsConsume)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsConsume  $modelsConsume
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsConsume $modelsConsume)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsConsume  $modelsConsume
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsConsume $modelsConsume)
    {
        //
    }
}
