<?php

namespace App\Http\Controllers;

use nusoap_client;
use GuzzleHttp\Client;
use App\ModelsTest;
//use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Request;



use SoapClient;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;



class TestController extends Controller


{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id = 1;
        $placa = "FOT438";
        $clase = "";
        $tipo_v = "";
        $cel = "3203500059";
        $cor = "prueba@gmail.com";
        $numero_i = "1017217135";
        $tipo_i = "CC";
        $nom = "Prueba";
        $codigo_sucursal = 87648;
        $codigo_punto = 2;
        $divpol = 05001;
        $codigo_red = 5444501;
        $clave = 54445;

        $fecha=date('Y-m-d');
        $hora=date('H:i:s');
        $fec_e=$fecha."T".$hora;

        $xml='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://AXAColpatria.Tarea.GestionPolizasSOAT.Cotizacion.Esquemas/CotizarPolizaSOATClienteReq/V1.0">
            <soapenv:Header/>
            <soapenv:Body>
            <v1:CotizarPolizaSOATClienteReq>
                <Header>
                    <Canal>Cajero</Canal>
                    <IdCorrelacionConsumidor>'.$id.'</IdCorrelacionConsumidor>               
                    <IdTransaccion>'.$id.'</IdTransaccion>
                    <PeticionFecha>'.$fec_e.'</PeticionFecha>               
                    <Usuario></Usuario>
                </Header>
                <Body>
                    <Vehiculo>
                        <Placa>'.$placa.'</Placa>                  
                        <Clase>'.$clase.'</Clase>                  
                        <Tipo>'.$tipo_v.'</Tipo>
                    </Vehiculo>
                    <Cliente>
                        <Cliente_TYPE>
                        <PersonaDetalle>
                            <ContactoPrincipal>
                                <CelularPersonal>
                                    <Numero>'.$cel.'</Numero>
                                </CelularPersonal>
                                <Email>
                                    <Email>'.$cor.'</Email>
                                </Email>
                            </ContactoPrincipal>
                            <Persona>
                                <Identificacion>
                                    <Identificacion>'.$numero_i.'</Identificacion>
                                    <TipoIdentificacion>'.$tipo_i.'</TipoIdentificacion>
                                </Identificacion>                           
                                <DatosBasicos>
                                    <NombreCompleto>'.$nom.'</NombreCompleto>
                                </DatosBasicos>
                            </Persona>
                        </PersonaDetalle>
                        </Cliente_TYPE>
                        <CotizacionesAsociadas>
                        <Cotizacion>
                            <CodigoSucursalAlianza>'.$codigo_sucursal.'</CodigoSucursalAlianza>
                            <CodigoPuntoVentaAliado>'.$codigo_punto.'</CodigoPuntoVentaAliado>
                            <CodigoDivipola>'.$divpol.'</CodigoDivipola>
                            <TipoConsulta>PLACA</TipoConsulta>
                            <CodigoRed>'.$codigo_red.'</CodigoRed>
                            <ValorDescuento>0</ValorDescuento>
                        </Cotizacion>
                        </CotizacionesAsociadas>
                    </Cliente>               
                    <TipoProceso>'.utf8_encode("Emisi�n").'</TipoProceso>               
                    <Clave>'.$clave.'</Clave>
                </Body>
            </v1:CotizarPolizaSOATClienteReq>
            </soapenv:Body>
            </soapenv:Envelope>';

            
        $xml = file_get_contents("/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/views/request_consult.xml");
        $url = "https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";
        $cert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
        $key = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";

        $placap = "FOT47D";

        $header_array =  array(
            "Canal"=> "Online",
            "IdCorrelacionConsumidor"=> "1",
            "IdTransaccion" => "1234567890",
            "PeticionFecha"=> "2020-05-19T10:57:00",
            "Usuario" => "SBC",
            
            );

        $body_array =  array(
                    "Vehiculo" => array(
                    "Placa" => $placap,
                    "Clase" => "X",
                    "Tipo" => "X",
                    ),
                    "Cliente" => array(
                        "Cliente_TYPE" => array(
                            "PersonaDetalle" => array(
                                "ContactoPrincipal"=> array(
                                "CelularPersonal"=> array(
                                    "Numero" => "3203500059",
                                    ),
                                    "Email" => array(
                                        "Email" => "X",
                                        ),
                                ),
                                        "Persona" => array(
                                            "Identificacion" => array(
                                                "Identificacion" => "1017217135",
                                                "TipoIdentificacion" => "CC",
                                            ),
                                                "DatosBasicos" => array(
                                                "NombreCompleto" => "X",
                                                ),
                                        ),
                            ), 

                        ), 
                                    
                        "CotizacionesAsociadas" => array(
                            "Cotizacion"=> array(
                                "CodigoSucursalAlianza" => "87648",
                                "CodigoPuntoVentaAliado" => "01",
                                "CodigoDivipola" => "05001",
                                "TipoConsulta" => "PLACA",
                                "CodigoRed" => "5444501",
                                "ValorDescuento" => "0",
                            ), 
                        ),
                ),
                    "TipoProceso" => "Emisión",
                    "Clave" => "54445",
                );

        $opts = array(
            'ssl' => array('local_cert' => $cert,"local_pk" => $key,
            'Content_type' => 'text/xml;charset=utf-8',
            'protocol_version' => 1.0,
            //'Content-Length' => 1933,
            )
        );
        $params = array(
            'http' => array('protocol_version' => 1.0,
                            'header' => "Transfer-Encoding: chunked\r\n",
                            'Transfer-Encoding' => "chunked\r\n",
                            'keep_alive' => false,
                            'cache_wsdl' => WSDL_CACHE_NONE),
                            'protocol_version' => 1.0,
                    //'Content-Type' => 'text/xml;charset=UTF-8',

                    'keep_alive' => false,
                    'trace' => true,
                    'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                    'encoding' => 'utf-8',
                    //'verifypeer' => false,
                    //'verifyhost' => True, 
                    'soap_version' => SOAP_1_1,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'exception' => true,
                    //'Content-Length' => 1933,
                    'protocol_version' => 1.1,
                    //'Cteonnt-Length' => 1933,
                    'connection_timeout' => 1240000,
                    //'header' => "Transfer-Encoding: chunked\r\n", 
                    //'protocol_version' => 1.0,
                    //'trace' => 1, 
                    //'exceptions' => 1, 
                    //"connection_timeout" => 124000000, 
                    'stream_context' => stream_context_create($opts));
    
        try{            //tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl
            $client = new SoapClient("https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl",$params);
        
            //dd($client->__getTypes());
            //dd($client->CotizarPolizaSOATCliente(['Header' => $header_array,'Body' => $body_array]));   
            
            $result = $client->CotizarPolizaSOATCliente(['Header'=>$header_array,'Body'=>$body_array]);
            //$result = $client->__soapCall('CotizarPolizaSOATCliente',([$xml]));

            //$result = $client->__doRequest($xml,$url,"CotizarPolizaSOATCliente",SOAP_1_2);

            print(var_dump($result));
            ob_start();
            $result = ob_get_contents();
            ob_end_clean();
            return $result;
            
            
            //$response = $client->__doRequest($request,$url,"",SOAP_1_1);

            //$response = $client->CotizarPolizaSOATCliente(['Header'=>$header_array,'Body'=>$body_array]);
            //$json = json_encode($response);
            //$obj = json_decode($json);
                    
        }
        catch(SoapFault $fault) {
            echo '<br>'.$fault;
            echo $fault->getMessage();
        }

        $json_data = json_encode((array) $result);
        print_r($json_data);
        
        $json = json_decode($json_data,  true);
        var_dump($json);

        //$Criticidad = $json['Header']['Criticidad'];
        //$RtaCodCanal = $json['Header']['RtaCodCanal'];
        //$RtaDescCanal = $json['Header']['RtaDescCanal'];

        //* DataPerson - Response CotizarPolizaSOATCliente - Body


        //$celular = $json['Body']['Cliente']['Cliente_TYPE']['PersonaDetalle']['ContactoPrincipal']['CelularPersonal']['Numero'];
        //$email = $json['Body']['Cliente']['Cliente_TYPE']['PersonaDetalle']['ContactoPrincipal']['Email']['Email'];
        //$identificacion = $json['Body']['Cliente']['Cliente_TYPE']['PersonaDetalle']['Persona']['Identificacion']['Identificacion'];
        //$tipo_identificacion = $json['Body']['Cliente']['Cliente_TYPE']['PersonaDetalle']['Persona']['Identificacion']['TipoIdentificacion'];
        //$nombre = $json['Body']['Cliente']['Cliente_TYPE']['PersonaDetalle']['Persona']['DatosBasicos']['NombreCompleto'];
        //$clave = $json['Body']['Cliente']['CotizacionesAsociadas']['Cotizacion'][0]['Clave'];
        //$valor = $json['Body']['Cliente']['CotizacionesAsociadas']['Cotizacion'][0]['ValorToltal'];
        //$fecha_vigencia = $json['Body']['Cliente']['CotizacionesAsociadas']['Cotizacion'][0]['Vigencia']['FechaInicio'];
        //$fecha_final = $json['Body']['Cliente']['CotizacionesAsociadas']['Cotizacion'][0]['Vigencia']['FechaFinal'];
        
        
        
        //* DataVehicule - Response CotizarPolizaSOATCliente - Body **/
        //$numero_motor = $json['Body']['Vehiculo']['NumeroMotor'];
        //$numero_chasis = $json['Body']['Vehiculo']['NumeroChasis'];
        //$numero_marca = $json['Body']['Vehiculo']['Marca'];
        //$cilindraje = $json['Body']['Vehiculo']['Cilindraje'];
        //$modelo = $json['Body']['Vehiculo']['Modelo'];
        //$linea = $json['Body']['Vehiculo']['Linea'];
        //$clase = $json['Body']['Vehiculo']['Clase'];
        //$carga = $json['Body']['Vehiculo']['CapacidadCarga'];
        //$pasajeros = $json['Body']['Vehiculo']['CapacidadTotalPasajeros'];
        //$ciudad_matricula = $json['Body']['Vehiculo']['CiudadMatricula'];
        //$tipo_servicio = $json['Body']['Vehiculo']['TipoServicio'];

        //$json = $json_data;

        $Criticidad = "";
        $RtaCodCanal = "";
        $RtaDescCanal = "";

        return view('test', compact('json','Criticidad','RtaCodCanal','RtaDescCanal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    
    {
        $cert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
        $key = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";

        $id = 1;
        $placa = "IMW71E";
        $clase = "";
        $tipo_v = "";
        $cel = "3203500059";
        $cor = "prueba@gmail.com";
        $numero_i = "1017217135";
        $tipo_i = "CC";
        $nom = "Prueba";
        $codigo_sucursal = 87648;
        $codigo_punto = 2;
        $divpol = 05001;
        $codigo_red = 5444501;
        $clave = 54445;

        $fecha=date('Y-m-d');
        $hora=date('H:i:s');
        $fec_e=$fecha."T".$hora;

        $xml='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://AXAColpatria.Tarea.GestionPolizasSOAT.Cotizacion.Esquemas/CotizarPolizaSOATClienteReq/V1.0">
            <soapenv:Header/>
            <soapenv:Body>
            <v1:CotizarPolizaSOATClienteReq>
                <Header>
                    <Canal>Cajero</Canal>
                    <IdCorrelacionConsumidor>'.$id.'</IdCorrelacionConsumidor>               
                    <IdTransaccion>'.$id.'</IdTransaccion>
                    <PeticionFecha>'.$fec_e.'</PeticionFecha>               
                    <Usuario></Usuario>
                </Header>
                <Body>
                    <Vehiculo>
                        <Placa>'.$placa.'</Placa>                  
                        <Clase>'.$clase.'</Clase>                  
                        <Tipo>'.$tipo_v.'</Tipo>
                    </Vehiculo>
                    <Cliente>
                        <Cliente_TYPE>
                        <PersonaDetalle>
                            <ContactoPrincipal>
                                <CelularPersonal>
                                    <Numero>'.$cel.'</Numero>
                                </CelularPersonal>
                                <Email>
                                    <Email>'.$cor.'</Email>
                                </Email>
                            </ContactoPrincipal>
                            <Persona>
                                <Identificacion>
                                    <Identificacion>'.$numero_i.'</Identificacion>
                                    <TipoIdentificacion>'.$tipo_i.'</TipoIdentificacion>
                                </Identificacion>                           
                                <DatosBasicos>
                                    <NombreCompleto>'.$nom.'</NombreCompleto>
                                </DatosBasicos>
                            </Persona>
                        </PersonaDetalle>
                        </Cliente_TYPE>
                        <CotizacionesAsociadas>
                        <Cotizacion>
                            <CodigoSucursalAlianza>'.$codigo_sucursal.'</CodigoSucursalAlianza>
                            <CodigoPuntoVentaAliado>'.$codigo_punto.'</CodigoPuntoVentaAliado>
                            <CodigoDivipola>'.$divpol.'</CodigoDivipola>
                            <TipoConsulta>PLACA</TipoConsulta>
                            <CodigoRed>'.$codigo_red.'</CodigoRed>
                            <ValorDescuento>0</ValorDescuento>
                        </Cotizacion>
                        </CotizacionesAsociadas>
                    </Cliente>               
                    <TipoProceso>'.utf8_encode("Emisi�n").'</TipoProceso>               
                    <Clave>'.$clave.'</Clave>
                </Body>
            </v1:CotizarPolizaSOATClienteReq>
            </soapenv:Body>
            </soapenv:Envelope>';

        $url = "https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";


        $client = new Client([
            'base_url' => $url,
            'body' => $xml, 
            'method'=>'POST',]);
            

        $options = [
            'method'=>'POST',
            'headers' => [
                'Content-Type' => 'text/xml;charset=utf-8',
                'Accept' => 'text/xml;charset=utf-8',
            ],
            'body' => $xml,
            'cert' => $cert,
            'ssl_key' => $key,
            'SOAPAction'=> "CotizarPolizaSOATCliente",

        ];
        
        $response = $client->request('get', $url, $options);

        //$xml = $response->xml();
        

        if ($response->getBody()) {
            echo $response->getBody();
            
            // JSON string: { ... }
            print("SI TIENE BODY");
        }
        

        $body = $response->getBody()->getContents();

        $contents = (string) $body;

        if ($body = $response->getBody()) {
            echo $body;
            // Cast to a string: { ... }
            $body->seek(0);
            // Rewind the body
            $body->read(1024);
            // Read bytes of the body
        }

        try {
            $client->request('POST',$url,[
                'headers' => [
                    'http_errors' => false,
                    'Content-Type' => 'text/xml;charset=utf-8',
                    'Accept' => 'text/xml;charset=utf-8',
                ],
                'body' => $xml, 
                'cert' => $cert,
                'ssl_key' => $key,
                ['connect_timeout' => 1000]],
            );
        }
        
        catch (GuzzleHttp\Exception\ClientException $e) {
            Log :: error ($e-> getResponse () -> getBody () -> getContents ());
            ddd($e->getResponse()->getBody()->getContents());
         //   $response = $e->getResponse()->getBody()->getContents();
         //   throw $e;
        };

            var_dump($response);

        setlocale(LC_ALL, "en_US.UTF-8");

        $Criticidad = var_dump($contents);


        $responseXml = simplexml_load_string($body);

        print("ESTE ES".$responseXml);



        return view('test', compact('Criticidad'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $url = "https://pmms.axacolpatria.co:4217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";
        $cert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
        $key = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";
        

        $header_emit =  array(
            "Canal"=> "Online",
            "IdCorrelacionConsumidor"=> "1",
            "IdTransaccion" => "1234567890",
            "PeticionFecha"=> "2020-05-19T10:57:00",
            "Usuario" => "SBC",
            );
            
        $body_emit = array(
            "Vehiculo" => array(
               "Placa" => "XXX",
            ),
            "Cliente" => array(
               "Cliente_Type" => array(
                  "Persona" => array(
                     "Identificacion" => array(
                        "Identificacion" => "XXX",
                        "TipoIdentificacion" => "XXX",
         
                     ),
                  ),
                  "PersonaDetalle" => array(
                     "ContactoPrincipal" => array(
                        "CelularPersonal" => array(
                           "Numero" => "XXX",
                        ),
                        "Email" => array(
                           "Email" => "XXX"
                        ),
                     ),
                  ),
               ),
            ),
            "CodigoPunto" => "XX",
            "Origen" => "XX",
            "Clave" => "XX",
            "CodigoRed" => "XX",
            "ValorDescuento" => "XX",
         );

         $opts = array(
            'ssl' => array('local_cert' => $cert,"local_pk" => $key,
            'Content_type' => 'text/xml;charset=UTF-8',
            'protocol_version' => 1.0,
            'Content-Length' => 1933,
            )
        );
        $params = array(
            'Content_type' => 'text/xml;charset=UTF-8',
                    'encoding' => 'UTF-8',
                    'verifypeer' => false, 
                    'verifyhost' => True, 
                    'soap_version' => SOAP_1_1,
                    'Content-Length' => 1933,
                    'connection_timeout' => 1240000,
                    'stream_context' => stream_context_create($opts));
    
        try{
            $client = new SoapClient("https://pmms.axacolpatria.co:4217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl",$params);
            //dd($client->__getTypes());
            //dd($client->CotizarPolizaSOATCliente(['Header' => $header_array,'Body' => $body_array]));   
            
            $result = $client->EmitirPolizaComposicionSOAT(['Header'=>$header_array,'Body'=>$body_array]);
        }
        catch(SoapFault $fault) {
            echo '<br>'.$fault;
            echo $fault->getMessage();
        }

        $json_data = json_encode((array) $result);
        print_r($json_data);

        //* Response EmitirPolizaComposicionSOAT - Header **/
        
        $json = json_decode($json_data,  true);
        $Criticidad = $json['Header']['Criticidad'];
        $RtaCodCanal = $json['Header']['RtaCodCanal'];
        $RtaDescCanal = $json['Header']['RtaDescCanal'];

        //* Entidad Datos SOAT - Response EmitirPolizaComposicionSOAT - Body **/

        $CodigoEntidad = $json['Body']['SOAT']['VariablesSOAT']['CodigoEntidad'];
        $ValorContribucionFOSYGA = $json['Body']['SOAT']['ContribucionesTransferencias']['ValorContribucionFOSYGA'];
        $FechaInicio = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['Vigencia']['FechaInicio'];
        $FechaFinal = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['Vigencia']['FechaFinal'];
        $CodigoSucursal = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['CodigoSucursal'];
        $Productor = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['Productor'];
        $NumeroFormulario = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['NumeroFormulario'];
        $IdTarifa = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['IdTarifa'];
        $ImpPrima = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['ImpPrima'];
        $ImpPrimaTotal = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['ImpPrimaTotal'];
        $NumeroPoliza = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['NumeroPoliza'];
        $CiudadExpedicion = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['CiudadExpedicion'];
        $TasaRunt = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['TasaRunt'];
        $ValorDescuento = $json['Body']['SOAT']['DatosSoat']['DatoSoat']['ValorDescuento'];





        $json = "test2";

        return view('test', compact('json'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsTest $modelsTest)
    {
        $id = 1;
        $placa = "FOT40D";
        $clase = "";
        $tipo_v = "";
        $cel = "3203500059";
        $cor = "prueba@gmail.com";
        $numero_i = "1017217135";
        $tipo_i = "CC";
        $nom = "Prueba";
        $codigo_sucursal = 87648;
        $codigo_punto = 2;
        $divpol = 05001;
        $codigo_red = 5444501;
        $clave = 54445;

        $fecha=date('Y-m-d');
        $hora=date('H:i:s');
        $fec_e=$fecha."T".$hora;
        $REQUEST='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://AXAColpatria.Tarea.GestionPolizasSOAT.Cotizacion.Esquemas/CotizarPolizaSOATClienteReq/V1.0">
        <soapenv:Header/>
        <soapenv:Body>
        <v1:CotizarPolizaSOATClienteReq>
            <Header>
                <Canal>Cajero</Canal>
                <IdCorrelacionConsumidor>'.$id.'</IdCorrelacionConsumidor>               
                <IdTransaccion>'.$id.'</IdTransaccion>
                <PeticionFecha>'.$fec_e.'</PeticionFecha>               
                <Usuario></Usuario>
            </Header>
            <Body>
                <Vehiculo>
                    <Placa>'.$placa.'</Placa>                  
                    <Clase>'.$clase.'</Clase>                  
                    <Tipo>'.$tipo_v.'</Tipo>
                </Vehiculo>
                <Cliente>
                    <Cliente_TYPE>
                    <PersonaDetalle>
                        <ContactoPrincipal>
                            <CelularPersonal>
                                <Numero>'.$cel.'</Numero>
                            </CelularPersonal>
                            <Email>
                                <Email>'.$cor.'</Email>
                            </Email>
                        </ContactoPrincipal>
                        <Persona>
                            <Identificacion>
                                <Identificacion>'.$numero_i.'</Identificacion>
                                <TipoIdentificacion>'.$tipo_i.'</TipoIdentificacion>
                            </Identificacion>                           
                            <DatosBasicos>
                                <NombreCompleto>'.$nom.'</NombreCompleto>
                            </DatosBasicos>
                        </Persona>
                    </PersonaDetalle>
                    </Cliente_TYPE>
                    <CotizacionesAsociadas>
                    <Cotizacion>
                        <CodigoSucursalAlianza>'.$codigo_sucursal.'</CodigoSucursalAlianza>
                        <CodigoPuntoVentaAliado>'.$codigo_punto.'</CodigoPuntoVentaAliado>
                        <CodigoDivipola>'.$divpol.'</CodigoDivipola>
                        <TipoConsulta>PLACA</TipoConsulta>
                        <CodigoRed>'.$codigo_red.'</CodigoRed>
                        <ValorDescuento>0</ValorDescuento>
                    </Cotizacion>
                    </CotizacionesAsociadas>
                </Cliente>               
                <TipoProceso>'.utf8_encode("Emisi�n").'</TipoProceso>               
                <Clave>'.$clave.'</Clave>
            </Body>
        </v1:CotizarPolizaSOATClienteReq>
        </soapenv:Body>
        </soapenv:Envelope>';


        $xml = file_get_contents("/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/views/request_consult.xml");
        $url = "https://pmms.axacolpatria.co:4217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";
        $cert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
        $key = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com_key.pem";
        $key2 = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";

        $placap = "FOT42D";

        $Header =  array(
        "Header" => array(
            "Canal"=> "Online",
            "IdCorrelacionConsumidor"=> "1",
            "IdTransaccion" => "1234567890",
            "PeticionFecha"=> "2020-05-19T10:57:00",
            "Usuario" => "SBC",
        ),
        );

        $Body =  array(
        "Body" => array(
                    "Vehiculo" => array(
                    "Placa" => $placap,
                    "Clase" => "X",
                    "Tipo" => "X",
                    ),
                    "Cliente" => array(
                        "Cliente_TYPE" => array(
                            "PersonaDetalle" => array(
                                "ContactoPrincipal"=> array(
                                "CelularPersonal"=> array(
                                    "Numero" => "3203500059",
                                    ),
                                    "Email" => array(
                                        "Email" => "X",
                                        ),
                                ),
                                        "Persona" => array(
                                            "Identificacion" => array(
                                                "Identificacion" => "1017217135",
                                                "TipoIdentificacion" => "CC",
                                            ),
                                                "DatosBasicos" => array(
                                                "NombreCompleto" => "X",
                                                ),
                                        ),
                            ), 

                        ), 
                                    
                        "CotizacionesAsociadas" => array(
                            "Cotizacion"=> array(
                                "CodigoSucursalAlianza" => "87648",
                                "CodigoPuntoVentaAliado" => "01",
                                "CodigoDivipola" => "05001",
                                "TipoConsulta" => "PLACA",
                                "CodigoRed" => "5444501",
                                "ValorDescuento" => "0",
                            ), 
                        ),
                ),
                    "TipoProceso" => "Emisión",
                    "Clave" => "54445",
            ),
                );

        $opts = array(
            'ssl' => array('local_cert' => $cert,"local_pk" => $key,
            'ssl_method' => SOAP_SSL_METHOD_SSLv3,
            'Content_type' => 'text/xml;charset=UTF-8',
            'protocol_version' => 1.2,
            'Content-Length' => 1933,
            'header' => "Transfer-Encoding: chunked\r\n",
            'compression'   => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
            )
        );
        $params = array(
                    'keep_alive' => false,
                    'cache_wsdl' => WSDL_CACHE_BOTH,
                    'Content_type' => 'text/xml;charset=UTF-8',
                    'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                    'trace' => true,
                    'protocol_version' => 1.0,
                    'header' => "Transfer-Encoding: chunked\r\n",
                    'soap_version' => SOAP_1_2,
                    'ssl_method' => SOAP_SSL_METHOD_SSLv3,
                    //'Cteonnt-Length' => 1933,
                    'connection_timeout' => 500000, 
                    //'header' => "Transfer-Encoding: chunked\r\n", 
                    //'protocol_version' => 1.0,
                    //'trace' => 1, 
                    //'exceptions' => 1, 
                    //"connection_timeout" => 124000000, 
                    'stream_context' => stream_context_create($opts));
    
        try{
            //$client = new SoapClient("https://pmms.axacolpatria.co:4217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl",$params);

            $opts = array(
                'ssl' => array('local_cert' => $cert,"local_pk" => $key,
                'Content_type' => 'text/xml;charset=utf-8',
                'protocol_version' => 1.0,
                //'Content-Length' => 1933,
                )
            );
            $params = array(
                'http' => array('protocol_version' => 1.0,
                                'header' => "Transfer-Encoding: chunked\r\n",
                                'Transfer-Encoding' => "chunked\r\n",
                                'keep_alive' => false,
                                'cache_wsdl' => WSDL_CACHE_NONE),
                                'protocol_version' => 1.0,
                        //'Content-Type' => 'text/xml;charset=UTF-8',
    
                        'keep_alive' => false,
                        'trace' => true,
                        'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                        'encoding' => 'utf-8',
                        //'verifypeer' => false,
                        //'verifyhost' => True, 
                        'soap_version' => SOAP_1_1,
                        'cache_wsdl' => WSDL_CACHE_NONE,
                        'exception' => true,
                        //'Content-Length' => 1933,
                        'protocol_version' => 1.1,
                        //'Cteonnt-Length' => 1933,
                        'connection_timeout' => 1240000,
                        //'header' => "Transfer-Encoding: chunked\r\n", 
                        //'protocol_version' => 1.0,
                        //'trace' => 1, 
                        //'exceptions' => 1, 
                        //"connection_timeout" => 124000000, 
                        'stream_context' => stream_context_create($opts));

            $client2 = new nusoap_client("https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl",'wsdl');

            $cert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
            $key = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com_key.pem";
            $key2 = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3.key.pem";
            $cercert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/cert.crt";
            
            $certRequest = array(
                "sslcertfile" => $cert,
                "sslkeyfile" => $key2,
                //"sslcertfile" => "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem",
                //"sslkeyfile" => "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3.key.pem",
                //"passphrase" => "",
                'stream_context' => stream_context_create($opts),);
        

            $client2->setHeaders(array('Content-Type' => ' text/xml;charset=utf-8',
                'User-Agent' => 'Mozilla/1.22 (compatible; MSIE 5.01; PalmOS 3.0) EudoraWeb 2',
                'SOAPAction'=>'CotizarPolizaSOATCliente'));

            $client2->setCredentials('','', '',$certificate = $certRequest = array(
                "ssl_local_cert" => $cert,
                "sslkeyfile" => $key2,
                //"sslcertfile" => "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem",
                //"sslkeyfile" => "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3.key.pem",
                //"passphrase" => "",
                'verifypeer' => 0, 
                'verifyhost' => 2,));
            

            //dd($client->__getTypes()); 
            //dd($client->CotizarPolizaSOATCliente(['Header' => $header_array,'Body' => $body_array]));   
            
            //$result = $client->CotizarPolizaSOATCliente(['Header'=>$header_array,'Body'=>$body_array]);

            $result = $client2->call('CotizarPolizaSOATCliente',(['Header'=>$Header,'Body'=>$Body]));
            

            //* $do = $client2->send($xml);
            //$result = $client2->responseData;
            //    $err = $client2->getError();
            //    if ($err) {
             //       // Display the error
             //       echo '<h2>Network Timeout! </h2><pre></pre><b>Ooopsy! Kindly hit the button below and hit <b>accept/send/yes</b>  on all prompt request.</b><br /><br /><input type="button" value="Please Click here" onClick="document.location.reload(true);">';
              //  } else {
             //   }
              //  print($result);
                

            //$result = $client->__soapCall('CotizarPolizaSOATCliente',array(
                //'Header' => $Header,
            
            // $respuesta = $client2->call("CotizarPolizaSOATCliente", $xml, "");
            //if ($client2->getError())
            //{
            //    echo "<br/><br/>Error al llamar el metodo<br/> ".$client2->getError();
            //}
            //else
            //{
            //} 



            if ($client2->fault) {
                echo 'Error: ';
                print_r($result);
            } else {
                // check result
                $err_msg = $client2->getError();
                if ($err_msg) {
                    // Print error msg
                    echo 'Error: '.$err_msg;
                } else {
                    // Print result
                    echo 'Result: ';
                    print_r($result);
                }
            }
            
            //));
            //$result = $client2->call('CotizarPolizaSOATCliente',array(
             //   'Header' => $header_array,
              //  'Body'   => $body_array,
                //)
            //);
            print_r($result);
            //$response = $client->CotizarPolizaSOATCliente(['Header'=>$header_array,'Body'=>$body_array]);
            //$json = json_encode($response);
            //$obj = json_decode($json);
            
        }
        catch(SoapFault $fault) {
            echo '<br>'.$fault;
            echo $fault->getMessage();
        }

        $json_data = json_encode((array) $result);
        print_r($json_data);

        $json = $json_data;
        $Criticidad = $result;

        return view('test', compact('Criticidad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */

    public function __construct(SoapWrapper $soapWrapper)
    {
      $this->soapWrapper = $soapWrapper;
    }

    protected $soapWrapper;
    
    public function edit(ModelsTest $modelsTest)
    {
        // Laravel SoapClient Wrapper

        $header_array =  array(
            "Canal"=> "Online",
            "IdCorrelacionConsumidor"=> "1",
            "IdTransaccion" => "1234567890",
            "PeticionFecha"=> "2020-05-19T10:57:00",
            "Usuario" => "SBC",
            
            );

        $placap = "FOT45D";

        $body_array =  array(
                    "Vehiculo" => array(
                    "Placa" => $placap,
                    "Clase" => "X",
                    "Tipo" => "X",
                    ),
                    "Cliente" => array(
                        "Cliente_TYPE" => array(
                            "PersonaDetalle" => array(
                                "ContactoPrincipal"=> array(
                                "CelularPersonal"=> array(
                                    "Numero" => "3203500059",
                                    ),
                                    "Email" => array(
                                        "Email" => "X",
                                        ),
                                ),
                                        "Persona" => array(
                                            "Identificacion" => array(
                                                "Identificacion" => "1017217135",
                                                "TipoIdentificacion" => "CC",
                                            ),
                                                "DatosBasicos" => array(
                                                "NombreCompleto" => "X",
                                                ),
                                        ),
                            ), 

                        ), 
                                    
                        "CotizacionesAsociadas" => array(
                            "Cotizacion"=> array(
                                "CodigoSucursalAlianza" => "87648",
                                "CodigoPuntoVentaAliado" => "01",
                                "CodigoDivipola" => "05001",
                                "TipoConsulta" => "PLACA",
                                "CodigoRed" => "5444501",
                                "ValorDescuento" => "0",
                            ), 
                        ),
                ),
                    "TipoProceso" => "Emisión",
                    "Clave" => "54445",
                );

        $xml = file_get_contents("/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/views/request_consult.xml");
        $url = "https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";
        $cert = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
        $key = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";

        $this->soapWrapper->add('Cotizar', function ($service) {
            $service
              ->wsdl("https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl")
              ->trace(true)
              ->certificate("/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem")
              ->certificate("/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key")
              ->cache(WSDL_CACHE_NONE);}
            );

        $response = $this->soapWrapper->call('Cotizar.CotizarPolizaSOATCliente', [
                'Header'=>$header_array,'Body'=>$body_array]);
            
        var_dump($response);

            return view('test', compact('response'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsTest $modelsTest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsTest  $modelsTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsTest $modelsTest)
    {
        $url="https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";   

        $id = 1;
        $placa = "FOT46D";
        $clase = "";
        $tipo_v = "";
        $cel = "3203500059";
        $cor = "prueba@gmail.com";
        $numero_i = "1017217135";
        $tipo_i = "CC";
        $nom = "Prueba";
        $codigo_sucursal = 87648;
        $codigo_punto = 2;
        $divpol = 05001;
        $codigo_red = 5444501;
        $clave = 54445;
        
        $fecha=date('Y-m-d');
        $hora=date('H:i:s');
        $fec_e=$fecha."T".$hora;
        $REQUEST='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://AXAColpatria.Tarea.GestionPolizasSOAT.Cotizacion.Esquemas/CotizarPolizaSOATClienteReq/V1.0">
        <soapenv:Header/>
        <soapenv:Body>
        <v1:CotizarPolizaSOATClienteReq>
            <Header>
                <Canal>Cajero</Canal>
                <IdCorrelacionConsumidor>'.$id.'</IdCorrelacionConsumidor>               
                <IdTransaccion>'.$id.'</IdTransaccion>
                <PeticionFecha>'.$fec_e.'</PeticionFecha>               
                <Usuario></Usuario>
            </Header>
            <Body>
                <Vehiculo>
                    <Placa>'.$placa.'</Placa>                  
                    <Clase>'.$clase.'</Clase>                  
                    <Tipo>'.$tipo_v.'</Tipo>
                </Vehiculo>
                <Cliente>
                    <Cliente_TYPE>
                    <PersonaDetalle>
                        <ContactoPrincipal>
                            <CelularPersonal>
                                <Numero>'.$cel.'</Numero>
                            </CelularPersonal>
                            <Email>
                                <Email>'.$cor.'</Email>
                            </Email>
                        </ContactoPrincipal>
                        <Persona>
                            <Identificacion>
                                <Identificacion>'.$numero_i.'</Identificacion>
                                <TipoIdentificacion>'.$tipo_i.'</TipoIdentificacion>
                            </Identificacion>                           
                            <DatosBasicos>
                                <NombreCompleto>'.$nom.'</NombreCompleto>
                            </DatosBasicos>
                        </Persona>
                    </PersonaDetalle>
                    </Cliente_TYPE>
                    <CotizacionesAsociadas>
                    <Cotizacion>
                        <CodigoSucursalAlianza>'.$codigo_sucursal.'</CodigoSucursalAlianza>
                        <CodigoPuntoVentaAliado>'.$codigo_punto.'</CodigoPuntoVentaAliado>
                        <CodigoDivipola>'.$divpol.'</CodigoDivipola>
                        <TipoConsulta>PLACA</TipoConsulta>
                        <CodigoRed>'.$codigo_red.'</CodigoRed>
                        <ValorDescuento>0</ValorDescuento>
                    </Cotizacion>
                    </CotizacionesAsociadas>
                </Cliente>               
                <TipoProceso>'.utf8_encode("Emisi�n").'</TipoProceso>               
                <Clave>'.$clave.'</Clave>
            </Body>
        </v1:CotizarPolizaSOATClienteReq>
        </soapenv:Body>
        </soapenv:Envelope>';
        
        require_once $path =('/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/vendor/pear/http_request2/HTTP/Request2.php');
        
        $r = new HTTP_Request2($url,HTTP_Request2::METHOD_POST);
        
        $certpem = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
        $keyk = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";
        
        $cert=$certpem;  
        $key=$keyk;
        
        $r->setHeader(array('Content-Type' => ' text/xml;charset=utf-8',
        'ssl_local_cert'    => $certpem,
        'key' => $key,
        'User-Agent' => 'Mozilla/1.22 (compatible; MSIE 5.01; PalmOS 3.0) EudoraWeb 2',
        'SOAPAction'=>'CotizarPolizaSOATCliente'));
        
        $opts = array(
            'ssl' => array('local_cert' => $cert,"local_pk" => $key,
            'Content_type' => 'text/xml;charset=utf-8',
            'protocol_version' => 1.0,
            //'Content-Length' => 1933,
            )
        );
        
        $r->setAdapter('curl');
        
        $r->setConfig(array(
        'ssl_local_cert' => $certpem,
        'ssl_private_key' => $key,
        'ssl_passphrase' => null,
        
        
         ));
        
        $r->setContentType = 'Content-Type: text/xml; charset=utf-8';
        
        //$r->setSslOptions(array('enabled'=>true,
        //'verifypeer' => 0, 
        //'verifyhost' => 2,
        //'cert' => $cert, 
        //'key' => $key
        //));
        
        
        //$r->setOption(array(
        //'Operation'=>'CotizarPolizaSOATCliente',
        //'Content-Lenght'=>strlen ($REQUEST), 
        //'Host'=>'www.w3schools.com',
        //'connection'=>'Keep-Alive',				
        //'timeout'=>90,
        //'connecttimeout'=>90
        //));
   
        $r->setBody($REQUEST);
            
        $result = $r->send();
        print("TIPO".gettype($result));
        $result_data = var_dump($result->getBody());
        print(var_dump($result->getBody("ValorToltal")));
        $Criticidad = $result;

        return view('test', compact('Criticidad'));}
}
