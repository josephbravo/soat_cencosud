<?php

namespace App\Http\Controllers;

use App\ModelsHome;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsHome  $modelsHome
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsHome $modelsHome)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsHome  $modelsHome
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsHome $modelsHome)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsHome  $modelsHome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsHome $modelsHome)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsHome  $modelsHome
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsHome $modelsHome)
    {
        //
    }
}
