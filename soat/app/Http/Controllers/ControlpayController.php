<?php

namespace App\Http\Controllers;

use App\Models\Data_pay_reports;
use App\Models\Cotizaciones;

use App\ModelsControlpay;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ControlpayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //Add Session to: requestId - placa & data person to emit

        $data_pay = Data_pay_reports::where('codigo_pto_venta','01')->get('requestid')->last();
        $data_pay_placa = Data_pay_reports::where('codigo_pto_venta','01')->get('placa')->last();
        $cambio = json_encode($data_pay["requestid"], true);
        $requestId = str_replace('"',"", $cambio);

        $cambio2 = json_encode($data_pay_placa["placa"], true);
        $placa = str_replace('"',"", $cambio2);

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
          } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
          } else {
            $nonce = mt_rand();
          }
          $nonceBase64 = base64_encode($nonce);

        $seed = date('c');
        $secretKey = 'css83lpw0bBc5jGw'; // trankey  
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $auth = '
        {
            "auth": {
                "login": "743eeee7c453349c31c09d82714f5c83",
                "tranKey": "'.$tranKey.'",
                "nonce": "'.$nonceBase64.'",
                "seed": "'.$seed.'"
            }
        }
        ';

        $req = json_decode($auth,true);

        $req["auth"]["tranKey"] = $tranKey;
        $req["auth"]["nonce"] = $nonceBase64;
        $req["auth"]["seed"] = $seed;

        $client = new Client([
            // You can set any number of default request options.
            'header'  => 'Content-type: application/json',
            'method'=>'POST',
            'json'=>$req,
            
            'timeout' => 600,
        ]);
        
        $response2 = $client->request('post', 'https://test.placetopay.com/redirection/api/session/'.$requestId, [ 'body' => json_encode($req) ]);

        $body2 = json_decode($response2->getBody()->getContents(), true);
        $status_resp = json_encode($body2['status']['status'], true);
        $status = str_replace('"',"", $status_resp);

        $status_resp = json_encode($body2['status']['reason'], true);
        $reason = str_replace('"',"", $status_resp);

        $status_resp = json_encode($body2['status']['message'], true);
        $message = str_replace('"',"", $status_resp);

        $value = json_encode($body2['request']['payment']['amount']['total'], true);

        $celphone_resp = json_encode($body2['request']['buyer']['mobile'], true);
        $celphone = str_replace('"',"", $celphone_resp);

        $description_resp = json_encode($body2['request']['payment']['description'], true);
        $description = str_replace('"',"", $description_resp);

        $refer = json_encode($body2['request']['payment']['reference'], true);
        $refer = str_replace('"',"", $refer);

        $date = date("d-m-Y");
        
        if ($status == "APPROVED"){
            $gracias = "GRACIAS";
            $mensaje = "TU COMPRA HA SIDO EXITOSA";
            $response="Transacción Aprobada";
            $option = "Tu bono será enviado a tu celular registrado en los próximos 3 días.";
            
            $data_pay = Data_pay_reports::where('requestId',$requestId)->update(['estado' => "APPROVED",
                                                                                'razon' => $reason,
                                                                                'mensaje' => $message]);

            $data_cotiz = Cotizaciones::where('placa',$placa)->update(['estado' => "RECHAZADO",
                                                                        'razon' => $reason,
                                                                        'mensaje' => $message]);                                                                
            }
        elseif ($status == "REJECTED"){
            $gracias = "";
            $mensaje="TU PAGO HA SIDO RECHAZADO";
            $response="Transacción Rechazada";
            $option = "Puedes intentar el pago de nuevo aquí:";

            $data_pay = Data_pay_reports::where('requestId',$requestId)->update(['estado' => "RECHAZADO",
                                                                                 'razon' => $reason,
                                                                                 'mensaje' => $message]);

            $data_cotiz = Cotizaciones::where('placa',$placa)->update(['estado' => "RECHAZADO",
                                                                        'razon' => $reason,
                                                                        'mensaje' => $message]);
        }
        elseif ($status == "PENDING"){
            $gracias = "";
            $mensaje="TU PAGO SE ENCUENTRA EN ESTADO PENDIENTE";
            $response="Transacción Pendiente";
            $option = "Texto cuando pago esta en estado pendiente";

            $data_pay = Data_pay_reports::where('requestId',$requestId)->update(['estado' => "PENDING_PAY",
                                                                                 'razon' => $reason,
                                                                                 'mensaje' => $message]);

            $data_cotiz = Cotizaciones::where('placa',$placa)->update(['estado' => "RECHAZADO",
                                                                       'razon' => $reason,
                                                                       'mensaje' => $message]);                                                                        
        }
        
        return view('controlpay', compact('gracias','mensaje','option', 'value', 'celphone', 'description', 'date', 'refer', 'response'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsControlpay  $modelsControlpay
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsControlpay $modelsControlpay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsControlpay  $modelsControlpay
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsControlpay $modelsControlpay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsControlpay  $modelsControlpay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsControlpay $modelsControlpay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsControlpay  $modelsControlpay
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsControlpay $modelsControlpay)
    {
        //
    }
}
