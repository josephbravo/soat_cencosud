<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');
Route::post('/home/store', 'HomeController@store')
    ->name('home.store');



Route::get('/question', 'QuestionController@index');
Route::post('/question/store', 'QuestionController@store')
    ->name('question.store');

Route::get('/terms', 'TermsController@index');

Route::get('/consume', 'ConsumeController@index')->name('consume');
Route::post('/consume/store', 'ConsumeController@store')
    ->name('consume.store');

Route::get('/confirm', 'ConfirmController@index')->name('confirm');
Route::post('/confirm/store', 'ConfirmController@store')
    ->name('confirm.store');

Route::get('/controlpay', 'ControlpayController@index')->name('controlpay');
Route::post('/controlpay/store', 'ControlPayController@store')
    ->name('controlpay.store');


Route::get('/test', 'TestController@index')->name('test');
Route::get('/test/store', 'TestController@store')->name('test.store');
Route::get('/test/show', 'TestController@show')->name('test.show');
Route::get('/edit', 'TestController@edit')->name('test.edit');
Route::get('/show', 'TestController@show')->name('test.show');
Route::get('/index', 'TestController@index')->name('test.index');
Route::get('/create', 'TestController@create')->name('test.create');
Route::get('/destroy', 'TestController@destroy')->name('test.destroy');



