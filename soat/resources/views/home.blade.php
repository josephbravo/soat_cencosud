@extends('base.init')

@section('script')
<script type="text/javascript" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/js/verify.js"></script>
 @endsection

@section('title')
<title>Home SOAT Cencosud</title>
 @endsection

@section('content')
<div class="home-page-back">
<div class="container-fluid">


<section class="home-page-info-ask">
<div class='p-preguntas'>
<a href="http://127.0.0.1:8001/question" aria-pressed="true"><p class="p-q">PREGUNTAS FRECUENTES <i class="far fa-question-circle"></i> </p></a>
</div>
<div class='p-preguntas-b'>
<p class="p-q-b">Los datos ingresados deben pertenecer al titular del vehiculo o moto, dando la cédula o el número de nit del titular. Recuerda que el celular y correo electrónico inscrito llegará la información de tu SOAT.</p>
</div>

</section>


<section class="home-page-info">
<div class="card text-center" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">Compra tu Soat en línea</h5>
    <h5 class="card-title2">Y llévate un descuento</h5>
  <div class="contact">
    <h2 class="number5">5%<img alt="portada" class="car" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Icono_auto.png" /><small class="very-small">Carro articular</small></h2>
    <h2 class="number3">3%<img alt="portada" class="motor" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Icono_moto.png" /></h2>
    <span class="terms">El descuento se realizará mediante un bono redimible en tiendas Jumbo, Metro y Estaciones de servicio Cencosud enviado vía sms en 4 días hábiles al celular instrito. </span>
  </div>
  </div>
</div>
</section>

<section class="home-page-form">
<div class="quoting soat3">
<div id="quoting-container" class="quoting-container soat3">
<div class="sc-caSCKo sXasC">

<form action="{{route('consume')}}" method="GET" name="form1" onsubmit="return verify()">
@csrf


     <h1 class="h1-gray">SOLICÍTALO</h1>
     <h1 class="h1-blue">AHORA</h1>
  <div class="form-group">
    <label class="text-form1">Ingresa tu placa</label>
    <input name="placa" id="input1" type="text" class="form-control" placeholder="ABC123" maxlength="6" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
  <div class="form-group">
     <label class="text-form1" for="exampleFormControlSelect1">Tipo documento</label>
     <select name="tipo_doc" class="form-control" id="exampleFormControlSelect1" required>
          <option value="CC">CC</option>
          <option value="CE">CE</option>
          <option value="NIT">NIT</option>
  </div>
     </select>

<div class="form-group">
     <label class="text-form1">Identificación</label>
     <input name="identificacion" type="text" class="form-control" maxlength="11" required>
</div>

<div class="btn-group">
     <div>
     <center>
     <input class="form-check-input-new" type="checkbox" id="inlineCheckbox1" value="option1" required="">
     </center>
     </div>&nbsp;
<div>
<center>
     <span class="terms">Acepto y autorizo el uso de mi información de acuerdo a los<span> </span><a href="http://127.0.0.1:8001/terms">términos y condiciones</a></span>
     <center></center></center></div>
</div>

<div class="form-group"><button type="submit" class="btn btn-primary">COTIZAR</button>
</div>
</form>

</div>
</section>

     </div>
</div> <!-- </div class="quoting soat3"> -->

<div class="funnel-footer-brands">
<div class="payment-methods">
<div class="contact"><h2 class="h2-blue">SIGUE SOLO ESTOS 3 PASOS</h2>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <img alt="portada" class="steps" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Icono-paso1.png" />
            <p class="text-steps">Llena los campos con tus datos personales o los de tu vehiculo o moto* </p>
        </div>
     <div class="col-md-4">
        <img alt="portada" class="steps" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Icono-paso3.png" />
        <p class="text-steps">Paga 100% online con tarjeta de crédito, debito o PSE. </p>
     </div>
     <div class="col-md-4">
        <img alt="portada" class="steps" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Icono-paso2.png" />
        <p class="text-steps">Recibirás el link de descarga de tu soat al celular registrado al momento de la compra</p>
     </div>
    </div>
</div>
</div>
<div class="contact"><h4 class="h4-celest">RECUERDA QUE PUEDE RENOVAR EL SOAT CON 6 MESES DE ANTICIPACIÓN</h4></div>
</div>
</div>

<?php
$url="https://tmms.axacolpatria.co:3217/AXAColpatria/Tarea/GestionPolizas/SOAT_V2/PolizaSOAT.svc?wsdl";   

$id = 1;
$placa = "FOT46D";
$clase = "";
$tipo_v = "";
$cel = "3203500059";
$cor = "prueba@gmail.com";
$numero_i = "1017217135";
$tipo_i = "CC";
$nom = "Prueba";
$codigo_sucursal = 87648;
$codigo_punto = 2;
$divpol = 05001;
$codigo_red = 5444501;
$clave = 54445;

$fecha=date('Y-m-d');
$hora=date('H:i:s');
$fec_e=$fecha."T".$hora;
$REQUEST='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://AXAColpatria.Tarea.GestionPolizasSOAT.Cotizacion.Esquemas/CotizarPolizaSOATClienteReq/V1.0">
<soapenv:Header/>
<soapenv:Body>
<v1:CotizarPolizaSOATClienteReq>
    <Header>
        <Canal>Cajero</Canal>
        <IdCorrelacionConsumidor>'.$id.'</IdCorrelacionConsumidor>               
        <IdTransaccion>'.$id.'</IdTransaccion>
        <PeticionFecha>'.$fec_e.'</PeticionFecha>               
        <Usuario></Usuario>
    </Header>
    <Body>
        <Vehiculo>
            <Placa>'.$placa.'</Placa>                  
            <Clase>'.$clase.'</Clase>                  
            <Tipo>'.$tipo_v.'</Tipo>
        </Vehiculo>
        <Cliente>
            <Cliente_TYPE>
            <PersonaDetalle>
                <ContactoPrincipal>
                    <CelularPersonal>
                        <Numero>'.$cel.'</Numero>
                    </CelularPersonal>
                    <Email>
                        <Email>'.$cor.'</Email>
                    </Email>
                </ContactoPrincipal>
                <Persona>
                    <Identificacion>
                        <Identificacion>'.$numero_i.'</Identificacion>
                        <TipoIdentificacion>'.$tipo_i.'</TipoIdentificacion>
                    </Identificacion>                           
                    <DatosBasicos>
                        <NombreCompleto>'.$nom.'</NombreCompleto>
                    </DatosBasicos>
                </Persona>
            </PersonaDetalle>
            </Cliente_TYPE>
            <CotizacionesAsociadas>
            <Cotizacion>
                <CodigoSucursalAlianza>'.$codigo_sucursal.'</CodigoSucursalAlianza>
                <CodigoPuntoVentaAliado>'.$codigo_punto.'</CodigoPuntoVentaAliado>
                <CodigoDivipola>'.$divpol.'</CodigoDivipola>
                <TipoConsulta>PLACA</TipoConsulta>
                <CodigoRed>'.$codigo_red.'</CodigoRed>
                <ValorDescuento>0</ValorDescuento>
            </Cotizacion>
            </CotizacionesAsociadas>
        </Cliente>               
        <TipoProceso>'.utf8_encode("Emisi�n").'</TipoProceso>               
        <Clave>'.$clave.'</Clave>
    </Body>
</v1:CotizarPolizaSOATClienteReq>
</soapenv:Body>
</soapenv:Envelope>';



require_once $path =('/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/vendor/pear/http_request2/HTTP/Request2.php');

$r = new HTTP_Request2($url,HTTP_Request2::METHOD_POST);

$certpem = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/894e9dc3991acb5f.pem";
$keyk = "/Users/josephbravo/Desktop/Projectos/Cencosud/soat/soat_cencosud/soat/resources/certs/zonic3_com.key";

$cert=$certpem;  
$key=$keyk;

$r->setHeader(array('Content-Type' => ' text/xml;charset=utf-8',
'ssl_local_cert'    => $certpem,
'key' => $key,
'User-Agent' => 'Mozilla/1.22 (compatible; MSIE 5.01; PalmOS 3.0) EudoraWeb 2',
'SOAPAction'=>'CotizarPolizaSOATCliente'));

$opts = array(
    'ssl' => array('local_cert' => $cert,"local_pk" => $key,
    'Content_type' => 'text/xml;charset=utf-8',
    'protocol_version' => 1.0,
    //'Content-Length' => 1933,
    )
);

$r->setAdapter('curl');

$r->setConfig(array(
'ssl_local_cert' => $certpem,
'ssl_private_key' => $key,
'ssl_passphrase' => null,


 ));

$r->setContentType = 'Content-Type: text/xml; charset=utf-8';

//$r->setSslOptions(array('enabled'=>true,
//'verifypeer' => 0, 
//'verifyhost' => 2,
//'cert' => $cert, 
//'key' => $key
//));


//$r->setOption(array(
//'Operation'=>'CotizarPolizaSOATCliente',
//'Content-Lenght'=>strlen ($REQUEST), 
//'Host'=>'www.w3schools.com',
//'connection'=>'Keep-Alive',				
//'timeout'=>90,
//'connecttimeout'=>90
//));

        
$r->setBody($REQUEST);
    
$result = $r->send();
print("TIPO".gettype($result));
$result_data = var_dump($result->getBody());
print(var_dump($result->getBody("ValorToltal")));
$Criticidad = $result;


?>  
@endsection