<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    @yield('script')
    <script src="https://kit.fontawesome.com/f93929b876.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Styles -->
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" media="all" />
    <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    
    @yield('title')
  </head>
  <body>
  <header>

  <img alt="portada" class="header-soat" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/HeaderSeguros.png" />

  </header>

  @yield('content')

  <footer class="funnel-footer">
    <div class="contact">

    <div class="row">
    <div class="col-sm-3"><img alt="portada" class="logos" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Logo-AXA.png" /></div>
    <div class="col-sm-2"><a href="https://www.placetopay.com/web/home"><img alt="portada" class="logos-ptp" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Logo+PTP.png" /></a></div>
    <div class="col-sm-2"><img alt="portada" class="logos-mc" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Logo-MC.png" /></div>
    <div class="col-sm-2"><img alt="portada" class="logos" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Logo-VISA.png" /></div>
    <div class="col-sm-3"><img alt="portada" class="logos" src="https://connect-static-files.s3.amazonaws.com/soat_cencosud/imagenes/Logo-Cencosud.png" /></div>
    </div>
    
    </div>

    </footer>


  </body>
</html>

