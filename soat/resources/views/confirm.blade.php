@extends('base.init')

@section('title')
<title>Confirm data SOAT Cencosud</title>
 @endsection

@section('content')
<div class="home-page-back-consume">
<div class="container-fluid-consume">

<section class="home-page-info">
</section>

<section class="home-page-form-confirm">
<div class="quoting soat3">
<div id="quoting-container" class="quoting-container soat3">
<div class="sc-caSCKo sXasC">

<form action="{{route('confirm.store')}}" method="post" name="form1">
@csrf

<div class="div1">
<div class="form-group-vehicule-confirm">     
<p class="p-vehicule">Ingresa tus datos personales</p>

<div class="form-group">
    <label class="text-form1">Correo electrónico</label>
    <input name="email" type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
  </div>

  <div class="form-group">
     <label class="text-form1">Celular</label>
     <input name="celular" type="text" class="form-control" maxlength="10" required>
</div>

  <div class="form-group">
    <label for="exampleFormControlSelect1" class="text-form1">¿Cómo nos conociste?</label>
    <select name="respuesta" class="form-control">
      <option>Facebook</option>
      <option>Publicidad</option>
      <option>Internet</option>

    </select>
  </div>
     <span class="terms">Recuerda que el correo electrónico y celular que ingreses será donde llegará la póliza y tu bono de regalo</span>
     </select>
</div>
</div>
<div class="div2">
  <div class="form-group-vehicule-confirm">
     <p class="p-vehicule">Características de tu vehículo</p>

      <p class="p-datas">Placa:&nbsp; XSM59D</p>
      <p class="p-datas">Marca:&nbsp; KYMCO</p>
      <p class="p-datas">Línea:&nbsp; AGILITY DIGITAL 125</p>
      <p class="p-datas">Modelo:&nbsp; 2016</p>
      <p class="p-datas">Cilindraje:&nbsp; 124</p>
      <p class="p-datas">Nombre:&nbsp; PAOLA ALEJANDRA ROJAS ZULUAGA</p>

     <p class="h2-gray-confirm">VALOR A PAGAR</p>
     <h1 class="h1-blue-value-confirm">$483.950</h1>

  </div>
     </select>

</div>

<div class="form-group"><button type="submit" class="btn-confirm">Comprar</button>
</div>
     
</form>

</div>
</section>

     </div>
</div> <!-- </div class="quoting soat3"> -->

@endsection